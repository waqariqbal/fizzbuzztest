﻿using System;
using System.CodeDom;

namespace FizzBuzz
{
    public class GameConsoleOutput : IGameOutput
    {
        public void WriteOutput(string output)
        {
            Console.WriteLine(output);
        }
    }
}
