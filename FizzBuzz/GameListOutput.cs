﻿using System.Collections.Generic;

namespace FizzBuzz
{
    public class GameListOutput : IGameOutput
    {
        private List<string> _results = new List<string>();

        public GameListOutput(List<string> results)
        {
            _results = results;
        }

        public void WriteOutput(string output)
        {
            _results.Add(output);
        }
    }
}
