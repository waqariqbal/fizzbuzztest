﻿namespace FizzBuzz
{
    public class GameLoop
    {
        private readonly IGameLogic _gameLogic;
        private readonly IGameOutput _gameOutput;

        public GameLoop(IGameLogic gameLogic, IGameOutput gameOutput)
        {
            _gameLogic = gameLogic;
            _gameOutput = gameOutput;
        }

        public void Excute(int start, int end)
        {
            for (int i = start; i <= end; i++)
            {
                _gameOutput.WriteOutput(_gameLogic.GetGameResult(i));
            }
        }
    }
}
