﻿namespace FizzBuzz
{
    public interface IGameLogic
    {
        string GetGameResult(int input);
    }
}
