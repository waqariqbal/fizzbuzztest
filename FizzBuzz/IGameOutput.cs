﻿namespace FizzBuzz
{
    public interface IGameOutput
    {
        void WriteOutput(string output);
    }
}
