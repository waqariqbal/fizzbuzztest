﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        private static IGameLogic _fizzBuzz = new FizzBuzzRetriever();
        private static IGameOutput _gameOutput = new GameConsoleOutput();
        private static GameLoop _gameLoop = new GameLoop(_fizzBuzz, _gameOutput);

        static void Main(string[] args)
        {
            _gameLoop.Excute(1, 100);
        }
    }
}
