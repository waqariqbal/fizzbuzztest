﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FizzBuzz;

namespace FizzBuzzWeb.Controllers
{
    public class FizzBuzzController : ApiController
    {
        private static IGameLogic _fizzBuzz = new FizzBuzzRetriever();


        // GET api/fizzbuzz
        public IEnumerable<string> Get()
        {
            List<string> list = new List<string>();
            GameListOutput output = new GameListOutput(list);
            GameLoop gameLoop = new GameLoop(_fizzBuzz, output);
            gameLoop.Excute(1, 100);
            return list;
        }

        // GET api/fizzbuzz/5
        public string Get(int id)
        {
            return _fizzBuzz.GetGameResult(id);
        }
    }
}
