﻿using System;
using NUnit.Framework;
using FizzBuzz;

namespace Tests.Tests
{
    public class FizzBuzzTests
    {
        private FizzBuzzRetriever _fizzBuzz = new FizzBuzzRetriever();

        [TestCase(1, "1")]
        [TestCase(2, "2")]
        [TestCase(11, "11")]
        [TestCase(52, "52")]
        [TestCase(88, "88")]
        public void NeitherFizzOrBuzzReturnNumber(int num, string result)
        {
            Assert.That(result, Is.EqualTo(_fizzBuzz.GetGameResult(num)));
        }

        [TestCase(3, "Fizz")]
        [TestCase(12, "Fizz")]
        [TestCase(51, "Fizz")]
        [TestCase(81, "Fizz")]
        [TestCase(99, "Fizz")]
        public void MultiplesOfThreeReturnFizz(int num, string result)
        {
            Assert.That(result, Is.EqualTo(_fizzBuzz.GetGameResult(num)));
        }

        [TestCase(5, "Buzz")]
        [TestCase(10, "Buzz")]
        [TestCase(35, "Buzz")]
        [TestCase(85, "Buzz")]
        [TestCase(100, "Buzz")]
        public void MultiplesOfFiveReturnBuzz(int num, string result)
        {
            Assert.That(result, Is.EqualTo(_fizzBuzz.GetGameResult(num)));
        }

        [TestCase(15, "FizzBuzz")]
        [TestCase(30, "FizzBuzz")]
        [TestCase(45, "FizzBuzz")]
        [TestCase(60, "FizzBuzz")]
        [TestCase(90, "FizzBuzz")]
        public void MultiplesOfThreeAndFiveReturnFizzBuzz(int num, string result)
        {
            Assert.That(result, Is.EqualTo(_fizzBuzz.GetGameResult(num)));
        }
    }
}
